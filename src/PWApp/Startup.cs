﻿using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PWApp.Api.Common.Contracts;
using PWApp.Api.DataAccess;
using PWApp.Api.Domain;
using PWApp.Api.Services;
using PWApp.Api.Tools;
using PWApp.WebApi.Common;

namespace PWApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddAuthentication();
            Mapper.Initialize(cfg =>
            {
                //мапинг из Api
                var apiAssembly = typeof(User).GetTypeInfo().Assembly;
                foreach (var profile in apiAssembly.GetTypes().Where(t => typeof(Profile).IsAssignableFrom(t)))
                {
                    cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                }
            });
            
            services.AddSingleton<IConfiguration>(Configuration);
           
            services.AddSingleton<IDataProviderFactory, PWAppDataProviderFactory>();


            //инъекция бизнес-слоя
            var businessAssembly = typeof(IUserService).GetTypeInfo().Assembly;
            var interfaces = businessAssembly.GetTypes().Where(t => t.GetTypeInfo().IsInterface && t.GetInterfaces().Any(i => i == typeof(IService))).ToList();
            var classes = businessAssembly.GetTypes().Where(t => t.GetTypeInfo().IsClass && t.GetInterfaces().Any(i => i == typeof(IService))).ToList();
            foreach (var inter in interfaces)
            {
                var type = classes.FirstOrDefault(t => t.GetInterfaces().Any(i => i == inter));
                if (type != null)
                    services.AddTransient(inter, type);
            }
            services.AddTransient<IAuthorization<User>>(provider => new UserService(provider.GetRequiredService<IDataProviderFactory>()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IDataProviderFactory dataProviderFactory, IUserService userService, ITransactionService transactionService)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseOAuthValidation();
            app.UseOpenIdConnectServer(options =>
            {
                // Create your own authorization provider by subclassing
                // the OpenIdConnectServerProvider base class.
                options.Provider = new AuthorizationProvider();

                // Enable the authorization and token endpoints.
                options.AuthorizationEndpointPath = "/connect/authorize";
                options.TokenEndpointPath = "/connect/token";

                // During development, you can set AllowInsecureHttp
                // to true to disable the HTTPS requirement.
                options.AllowInsecureHttp = true;

                // Register a new ephemeral key, that is discarded when the application
                // shuts down. Tokens signed using this key are automatically invalidated.
                // This method should only be used during development.
                options.SigningCredentials.AddEphemeralKey();

                // Note: uncomment this line to issue JWT tokens.
                // options.AccessTokenHandler = new JwtSecurityTokenHandler();

                //Tokens lifetime
                options.AccessTokenLifetime = TimeSpan.FromHours(9);
                options.RefreshTokenLifetime = TimeSpan.FromHours(12);
            });

            app.UseDefaultFiles(); // serves up our wwwroot files
            app.UseStaticFiles(); // allows serving of static files
            app.UseMvc();

            DbInitializer.Initialize(dataProviderFactory, userService, transactionService);
        }
    }
}
