﻿using System.Security.Claims;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PWApp.WebApi.Controllers.Common;
using AspNet.Security.OpenIdConnect.Extensions;

namespace PWApp.WebApi.Controllers
{
    [Route("[controller]")]
    public class ConnectController : BaseController
    {
        [Authorize(ActiveAuthenticationSchemes = OpenIdConnectServerDefaults.AuthenticationScheme)]
        [HttpGet]
        [Route("Userinfo")]
        public IActionResult Get()
        {
            return Json(new
            {
                sub = User.GetClaim(ClaimTypes.NameIdentifier),
                name = User.GetClaim(ClaimTypes.Name)
            });
        }

        /// <summary>
        /// Проверить авторизован ли пользователь
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("CheckAuthenticationOnServer")]
        public IActionResult CheckAuthorization()
        {
            return Ok();
        }
    }
}
