﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Exceptions;
using PWApp.Api.Domain;
using PWApp.Api.Dto;
using PWApp.Api.Services;
using PWApp.WebApi.Controllers.Common;

namespace PWApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : BaseController, IRestfulService<UserDto, Guid>
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion
        #region Constructors

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        #endregion
        #region IRestfulService Implementation

        /// <summary>
        /// Save(Create) user
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] UserDto item)
        {
            var usr = new User(item.Email, item.Password)
            {
                Balance = item.Balance,
                Name = item.Name
            };

            try
            {
                usr = _userService.Create(usr);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<UserDto>(usr);

            return Ok(result);
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<User> usrs;
            try
            {
                usrs = new List<User>(_userService.GetAll());
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<IEnumerable<UserDto>>(usrs);

            return Ok(result);
        }

        /// <summary>
        /// Get single user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            User usr;
            try
            {
                usr = _userService.FindBy(f=>f.Id==id).FirstOrDefault();
                if (usr == null)
                {
                    throw new BusinessException("Пользователь не найден");
                }
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<UserDto>(usr);

            return Ok(result);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="updatedItem"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] UserDto updatedItem)
        {
            var usr = Mapper.Map<User>(updatedItem);
            try
            {
                usr = _userService.Update(usr);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<UserDto>(usr);

            return Ok(result);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _userService.Delete(id);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }

            return Ok();
        }

        #endregion
    }
}
