﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace PWApp.WebApi.Controllers.Common
{
    public abstract class BaseController : Controller
    {
        protected IMapper Mapper = new Mapper(AutoMapper.Mapper.Configuration);
    }
}
