﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Exceptions;
using PWApp.Api.Domain;
using PWApp.Api.Dto;
using PWApp.Api.Services;
using PWApp.WebApi.Controllers.Common;

namespace PWApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : BaseController, IRestfulService<TransactionDto, Guid>
    {
        #region Fields

        private readonly ITransactionService _transactionService;

        #endregion
        #region Constructors

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        #endregion
        #region IRestfulService Implementation

        /// <summary>
        /// Save(Create) transaction
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] TransactionDto item)
        {
            var transaction = Mapper.Map<Transaction>(item);
            try
            {
                _transactionService.CreatePair(transaction);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }

            return Ok();
        }

        /// <summary>
        /// Get all transactions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Transaction> transactions;
            try
            {
                transactions = new List<Transaction>(_transactionService.GetAll());
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<IEnumerable<TransactionDto>>(transactions);

            return Ok(result);
        }

        /// <summary>
        /// Получить список всех событий
        /// </summary>
        /// <returns></returns>
        [HttpGet("{ownerId}")]
        [Route("ByUser")]
        public IActionResult GetByUser(Guid ownerId)
        {
            IEnumerable<Transaction> transactions;
            try
            {
                transactions = new List<Transaction>(_transactionService.FindBy(f => (f.FromUserId == ownerId && f.Amount<0) || (f.ToUserId == ownerId && f.Amount > 0)));
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<IEnumerable<TransactionDto>>(transactions);

            return Ok(result);
        }

        /// <summary>
        /// Get single transaction
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            Transaction transaction;
            try
            {
                transaction = _transactionService.FindBy(f=>f.Id==id).FirstOrDefault();
                if (transaction == null)
                {
                    throw new BusinessException("Пользователь не найден");
                }
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<TransactionDto>(transaction);

            return Ok(result);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="updatedItem"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] TransactionDto updatedItem)
        {
            var transaction = Mapper.Map<Transaction>(updatedItem);
            try
            {
                transaction = _transactionService.Update(transaction);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<TransactionDto>(transaction);

            return Ok(result);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _transactionService.Delete(id);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }

            return Ok();
        }

        #endregion
    }
}
