﻿using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.Extensions.DependencyInjection;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Domain;
using PWApp.Api.Tools;

namespace PWApp.WebApi.Common
{
    /// <summary>
    /// Провайдер авторизации OpenId
    /// </summary>
    /// <see cref="http://kevinchalet.com/2016/07/13/creating-your-own-openid-connect-server-with-asos-creating-your-own-authorization-provider/"/>
    sealed class AuthorizationProvider : OpenIdConnectServerProvider
    {
        /// <summary>
        /// Обработчик события запроса информации о пользователе (в данном случае пропускает обработку и передает на самописный)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task HandleUserinfoRequest(HandleUserinfoRequestContext context)
        {
            // Note: by default, the OpenID Connect server middleware automatically handles
            // userinfo requests and directly writes the JSON response to the response stream.
            // Calling context.SkipToNextMiddleware() bypasses the default request processing
            // and delegates it to a custom ASP.NET Core MVC controller (UserinfoController).
            context.SkipToNextMiddleware();
            return Task.FromResult(0);
        }

        /// <summary>
        /// Обработчик запроса токена (тут пишется пользовательская обработка)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task HandleTokenRequest(HandleTokenRequestContext context)
        {
            var authService = context.HttpContext.RequestServices.GetRequiredService<IAuthorization<User>>();
            
            Claim userClaim;
            User user;
            if (context.Request.Username.IsValidEmail())
            {
                user = authService.FIndByEmail(context.Request.Username);
                if (user == null)
                {
                    context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Пользователь не найден.");
                    return;
                }
                if (!user.IsValidPassword(context.Request.Password))
                {
                    context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Пароль указан не верно.");
                    return;
                }
                userClaim = new Claim(ClaimTypes.Email, user.Email);
            }
            else
            {
                context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Введенный логин не соответствует маске адреса электронной почты.");
                return;
            }


            var identity = new ClaimsIdentity(context.Options.AuthenticationScheme);
            // Note: the name identifier is always included in both identity and
            // access tokens, even if an explicit destination is not specified.
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString("D")));
            identity.AddClaim(new Claim("Balance", user.Balance.ToString("N")));
            // When adding custom claims, you MUST specify one or more destinations.
            // Read "part 7" for more information about custom claims and scopes.
            userClaim.SetDestinations(OpenIdConnectConstants.Destinations.AccessToken,
                OpenIdConnectConstants.Destinations.IdentityToken);
            identity.AddClaim(userClaim);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.ToString()));
            
            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(
                new ClaimsPrincipal(identity),
                new AuthenticationProperties(),
                context.Options.AuthenticationScheme);
            // Set the list of scopes granted to the client application.
            ticket.SetScopes(
                /* openid: */ OpenIdConnectConstants.Scopes.OpenId,
                /* email: */ OpenIdConnectConstants.Scopes.Email,
                /* profile: */ OpenIdConnectConstants.Scopes.Profile);
            // Set the resource servers the access token should be issued for.
            ticket.SetResources("resource_server");
            
            context.Validate(ticket);
        }

        /// <summary>
        /// Implementing a policy skipping client authentication (for JS/mobile apps-only scenarios)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ValidateTokenRequest(ValidateTokenRequestContext context)
        {
            // Reject the token request that don't use grant_type=password or grant_type=refresh_token.
            if (!context.Request.IsPasswordGrantType() && !context.Request.IsRefreshTokenGrantType())
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                    description: "Only resource owner password credentials and refresh token " +
                                 "are accepted by this authorization server");
                return Task.FromResult(0);
            }
            // Since there's only one application and since it's a public client
            // (i.e a client that cannot keep its credentials private), call Skip()
            // to inform the server the request should be accepted without 
            // enforcing client authentication.
            context.Skip();
            return Task.FromResult(0);
        }

        /// <summary>
        /// Добавление кастомных параметров в ответ сервера
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ApplyTokenResponse(ApplyTokenResponseContext context)
        {
            // Only add the custom parameter if the response indicates a successful response.
            if (string.IsNullOrEmpty(context.Error))
            {
                context.Response["userName"] = context.Ticket.Principal.FindFirst(ClaimTypes.Name).Value;
                context.Response["email"] = context.Ticket.Principal.FindFirst(ClaimTypes.Email).Value;
                context.Response["userId"] = context.Ticket.Principal.FindFirst(ClaimTypes.NameIdentifier).Value;
                context.Response["balance"] = context.Ticket.Principal.FindFirst("Balance").Value;
            }
            return Task.FromResult(0);
        }
    }
}
