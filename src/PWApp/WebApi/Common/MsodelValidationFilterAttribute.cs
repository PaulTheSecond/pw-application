﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PWApp.WebApi.Common
{
    /// <summary>
    /// Атрибут валидации модели
    /// </summary>
    public class MsodelValidationFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid) return;

            var errors = context.ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage)).Distinct().ToArray();
            context.Result = new BadRequestObjectResult(new { errors });
        }
    }
}
