﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .factory('paginationFactory', paginationFactory);

    function paginationFactory() {
        /**
        * Constructor
        * @returns {} service instance
        */
        function service() {
            this.pageSize = 10;
            this.currentPage = 1;
            this.totalPages = 0;
            this.numberOfPages = _numberOfPages;
            this.maxSize = 10;
        };

        return new service();

        function _numberOfPages() {
            return Math.ceil(this.totalPages / this.pageSize);
        }
    }
})();