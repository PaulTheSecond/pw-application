﻿(function() {
    'use strict';

// Repository 
    var app = angular.module('pwApp');
    app.service("ajaxServiceFactory", ajaxServiceFactory);
    app.service("addictionAjaxServiceFactory", addictionAjaxServiceFactory);

    ajaxServiceFactory.$inject = ['$resource'];
    addictionAjaxServiceFactory.$inject = ['$resource'];

    function ajaxServiceFactory($resource) {

        //// PUBLIC METHODS - Definition

        this.getService = _getService;

        //// PUBLIC METHODS - Implementation

        function _getService(endPoint) {
            if (endPoint === '') {
                throw "Invalid end point";
            }

            // create resource to make AJAX calls
            return $resource(endPoint + '/:id',
                {
                    id: '@Id'
// default URL params, '@' Indicates that the value should be obtained from a data property 
                },
                {
                    'update': { method: 'PUT' } // add update to actions (is not defined by default)
                });
        }
    }

    function addictionAjaxServiceFactory($resource) {
        this.getService = _getService;

        //// PUBLIC METHODS - Implementation

        function _getService(endPoint, byOwnerMethodName) {
            if (endPoint === '' && byOwnerMethodName === '') {
                throw "Invalid end point";
            }

            // create resource to make AJAX calls
            return $resource(endPoint + '/:id',
                {
                    id: '@id'
                    // default URL params, '@' Indicates that the value should be obtained from a data property 
                },
                {
                    'update': { method: 'PUT' }, // add update to actions (is not defined by default)
                    byOwner: { method: 'GET', params: { ownerId: '@ownerId' }, isArray: true, url: endPoint + byOwnerMethodName }
                });
        }
    }
})();