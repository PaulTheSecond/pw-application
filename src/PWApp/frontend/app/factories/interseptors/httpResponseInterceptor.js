﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .factory('httpResponseInterceptor', httpResponseInterceptor);

    httpResponseInterceptor.$inject = ['$q', '$location', 'localStorageService', '$rootScope'];

    /**
     * Перехватчик Http-ответов
     * see in http://www.codeproject.com/Articles/806029/Getting-started-with-AngularJS-and-ASP-NET-MVC-Par
     * @param {} $q 
     * @param {} $location 
     * @returns {} 
     */
    function httpResponseInterceptor($q, $location, localStorageService, $rootScope) {
        return {
            response: function (response) {
                if (response.status === 401) {
                    $rootScope.logOut();
                    $location.path('/login');
                }
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                switch (rejection.status) {
                    case 401:
                        $rootScope.logOut();
                        $location.path('/login');
                        break;
                    case 404:
                        $location.path('/404');
                        break;
                    case 500:
                        $location.path('/500');
                }
                return $q.reject(rejection);
            },
            request: function (config) {
                config.headers = config.headers || {};

                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                }

                return config;
            }
        };
    }
})();