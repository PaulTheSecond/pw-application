﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .factory('catchService', catchService);
    catchService.$inject = ['$location', 'Notification', '$timeout', '$window'];

    function catchService($location, notification, $timeout, $window) {
        var service = {
            catchException: catchException,
            catchSuccessWithRedirect: _catchSuccessWithRedirect
        };

        return service;
        /**
         * Обработка ошибок
         * @param {} msgType тип сообщения
         * @param {} response ответ от сервера
         * @param {} shownMessage отображаемое сообщение
         * @returns {} 
         */
        function catchException(response, shownMessage, title) {
            var errors = [];
            if (response.msg) {
                var i;
                if (response.msg.errors) {
                    for (i = 0; i < response.msg.errors.length; i++) {
                        errors.push(response.msg.errors[i]);
                    }
                }
                if (response.msg.modelState) {
                    for (var key in response.msg.modelState) {
                        if (response.msg.modelState.hasOwnProperty(key)) {
                            for (i = 0; i < response.msg.modelState[key].length; i++) {
                                errors.push(response.msg.modelState[key][i]);
                            }
                        }
                    }
                }
                if (response.msg.resultMessage) {
                    errors.push(response.msg.resultMessage);
                }
                if (response.msg.error_description) {
                    errors.push(response.msg.error_description);
                }
                if (response.msg.data) {
                    errors.push(response.msg.data);
                }
                if (response.msg && !response.msg.errors && !response.msg.modelState && !response.msg.error_description && !response.msg.data) {
                    errors.push(response.msg);
                }
                notification.error({ message: shownMessage + errors.join(' '), title: title ? title : '' });
            } else {
                $location.path('/500');
            }
        }

        /**
         * Сообщение об успехе с редиректом
         * @param {} redirectUrl адрес для редиректа
         * @param {} msg сообщение
         * @param {} title Заголовок
         * @returns {} 
         */
        function _catchSuccessWithRedirect(redirectUrl, msg, title) {
            notification.success({ message: msg, title: title });
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $window.location.href = '/#!' + redirectUrl;
            }, 2000);
        }
    }
})();