﻿(function () {
    'use strict';

    var module = angular.module('fieldValidationModule', [
        // Angular modules 
        'ngRoute'
        // Custom modules 

        // 3rd Party Modules
        
    ]);
    module.directive('sameAs', sameAsDirective);
    
    /**
     * Дирекетива сравнения "такой как"
     */
    function sameAsDirective() {
        // Usage:
        //     <sameAsDirective></sameAsDirective>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            require: 'ngModel'
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            ngModel.$validators.sameAs = function (modelValue, viewValue) {
                return modelValue === scope.$eval(attrs.sameAs);
            };

            scope.$on('destroy', scope.$watch(attrs.sameAs, function () {
                ngModel.$validate();
            }));
        }
    }

    module.directive('validationRequiredDirective', validationRequiredDirective);

    /**
     * Директива необходимости валидации
     * @returns {} 
     */
    function validationRequiredDirective() {

        var directive = {
            link: function (scope, element, attrs, mCtrl) {
                function myValidation(value) {
                    if (value) {
                        if (validate(value)) {
                            mCtrl.$setValidity('requreFormat', true);
                            scope.validModel = true;
                        } else {
                            mCtrl.$setValidity('requreFormat', false);
                            scope.validModel = false;
                        }
                    }
                    return value;
                }
                mCtrl.$parsers.push(myValidation);
            },
            require: 'ngModel'
        };
        return directive;
    };

    function validate(login) {
        var email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return email.test(login);
    }

})();