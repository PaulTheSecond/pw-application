﻿(function () {
    'use strict';

    var module = angular.module('AuthServices', [
        // Angular modules 
        'ngRoute',
        'ngResource',
        // Custom modules 

        // 3rd Party Modules
        'LocalStorageModule'
    ]);

    //Сервис авторизации
    module.factory('auth', auth);

    auth.$inject = ['$http', '$q', 'localStorageService', '$rootScope', '$route', '$location','$resource'];

    function auth($http, $q, localStorageService, $rootScope, $route, $location,$resource) {
        var authServiceFactory = { registration: {} };

        var _userService = $resource('api/User/:id',
                {
                    id: '@Id'
                },
                {
                    'update': { method: 'PUT' } // add update to actions (is not defined by default)
                });

        var _authentication = {
            isAuth: false,
            userName: ""
        };

        var _login = function (loginData) {

            var data = "grant_type=password&username=" +
            loginData.login + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post('connect/token', data, {
                headers:
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                localStorageService.set('authorizationData',
                {
                    token: response.data.access_token,
                    userName: response.data.userName,
                    userId: response.data.userId,
                    email: response.data.email,
                    balance: response.data.balance
                });

                _authentication.isAuth = true;
                _authentication.userName = response.data.userName;
                _authentication.userId = response.data.userId;
                _authentication.email = response.data.email;
                _authentication.balance = response.data.balance;

                deferred.resolve(response);
            }, function (err) {
                _logOut();
                deferred.reject(createErrorObject(err, 'err'));
            });

            return deferred.promise;
        };

        /**
         * Выполнить выход пользователя
         */
        var _logOut = function () {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = '';
            _authentication.userId = '';
            _authentication.email = '';
            _authentication.balance = null;
            var url = $location.url();
            if (url !== '/singin' && url !== '/singup') {
                $route.reload();
                $location.path('/singin');
            }
        };

        /**
         * Проверка требования прав для шаблона
         * @param {} view 
         * @returns {} 
         */
        var _checkPermissionForView = function (view) {
            if (!view.requiresAuthentication) {
                return true;
            }

            return userHasPermissionForView(view);
        };

        /**
         * проверка пользователя на наличие прав для отображения области
         * @param {} view 
         * @returns {} 
         */
        var userHasPermissionForView = function (view) {
            if (!_authentication.isAuth) {
                return false;
            }

            if (!view.permissions || !view.permissions.length) {
                return true;
            }

            return auth.userHasPermission(view.permissions);
        };

        /**
         * Заполнить данные авторизации из локального хранилища
         * @returns {} 
         */
        var _fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.userId = authData.userId;
                _authentication.email = authData.email;
                _authentication.balance = authData.balance;
            }
        };

        /**
         * проверка авторизации на сервере
         * @returns {} 
         */
        var _checkAuth = function () {
            $http.get('connect/CheckAuthenticationOnServer');
        }

        /**
         * Зарегистрировать пользователя в системе
         * @param {} registration данные регистрации
         */
        var _saveRegistration = function (registration) {

            _logOut();

            return $http.post('api/user', registration).then(function (response) {
                return response;
            });
        };

        var _updateBalance = function() {
            _userService.get({ id: _authentication.userId }, // success response
                function(response) { _authentication.balance = response.balance });
        }

        authServiceFactory.registration.save = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logOut = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;
        authServiceFactory.checkPermissionForView = _checkPermissionForView;
        authServiceFactory.checkAuth = _checkAuth;
        authServiceFactory.updateBalance = _updateBalance;

        $rootScope.authentication = _authentication;
        $rootScope.logOut = _logOut;

        return authServiceFactory;
    }
})();