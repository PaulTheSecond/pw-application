﻿(function () {
    'use strict';

    var pwApp = angular.module('pwApp', [
        // Angular modules 
        'ngRoute',
        'ngLocale',
        'ngAnimate',
        'ngResource',
        'ngMessages',
        // Custom modules 
        'fieldValidationModule',
        'AuthServices',
        // 3rd Party Modules
        'ui.bootstrap',
        'ui.bootstrap.tpls',
        'LocalStorageModule',
        'angular-loading-bar',
        'ui-notification',
        'ngMaterial'
    ]);

    configFunction.$inject = ['$routeProvider', '$httpProvider', 'NotificationProvider', '$mdDateLocaleProvider', '$mdThemingProvider'];

    pwApp.config(configFunction);

    function configFunction($routeProvider ,$httpProvider, notificationProvider, $mdDateLocaleProvider, $mdThemingProvider) {
        $routeProvider
            .when('/', {
                controller: 'homeController',
                templateUrl: '/views/home/index.html',
                requiresAuthentication: true
            })
            .when('/singin', {
                controller: 'singinController',
                templateUrl: '/views/account/singin.html'
            })
            .when('/singup', {
                controller: 'singupController',
                templateUrl: '/views/account/singup.html'
            })
            .when('/transactions', {
                controller: 'transactionsController as controller',
                templateUrl: '/views/transactions/index.html',
                requiresAuthentication: true
            })
            .when('/addTransaction', {
                controller: 'addTransactionController as controller',
                templateUrl: '/views/transactions/addTransaction.html',
                requiresAuthentication: true
            })
            .when('/404', {
                templateUrl: '/views/error/fof.html'
            })
            .when('/500', {
                templateUrl: '/views/error/InternalServerError.html'
            })
            .otherwise({
                redirectTo: '/'
            });

        $httpProvider.interceptors.push('httpResponseInterceptor');
        $httpProvider.interceptors.push('timeoutHttpInterceptor');

        notificationProvider.setOptions({
            delay: 10000,
            startTop: 60,
            startRight: 10,
            verticalSpacing: 10,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'bottom'
        });

        initDatepickerLocale($mdDateLocaleProvider);

        $mdThemingProvider.theme('default');
    };

    pwApp.run(['auth', '$location', '$timeout', '$rootScope', function (auth, $location, $timeout, $rootScope) {

        auth.fillAuthData();

        /**
        * @event collapse navbar when click on item
        */
        $(document).on('click', '.navbar-collapse.in', function (e) {
            if ($(e.target).is('a')) {
                $(this).collapse('hide');
            }
        });

        /**
        * @event collapse navbar when click on window
        */
        $(window).on('click', function (e) {
            var $mainNavbar = $('#headerNavbar.navbar-collapse');
            var $target = $(e.target),
                $parent = $target.closest($mainNavbar);

            if (!$parent.length && $mainNavbar.is(':visible')) {
                $mainNavbar.collapse('hide');
            }
            var $tabNavbar = $('#mainTabs.navbar-collapse');
            $parent = $target.closest($tabNavbar);
            if (!$parent.length && $tabNavbar.is(':visible')) {
                $tabNavbar.collapse('hide');
            }

            $(window).trigger('resize');
        });

        $rootScope.$on('$routeChangeStart', function (event, next) {
            if (!auth.checkPermissionForView(next)) {
                event.preventDefault();
                $location.path("/singin");
            }
        });
        auth.checkAuth();

        if (auth.authentication.isAuth) {
            var url = $location.path();
            if (url === '/' || url === '') {
                $location.path('/');
            }
        }

        window.setInterval(auth.updateBalance(), 60000);
    }]);

    //We already have a limitTo filter built-in to angular,
    //let's make a startFrom filter
    //see in http://stackoverflow.com/questions/11581209/pagination-on-a-list-using-ng-repeat
    pwApp.filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            if (input) {
                return input.slice(start);
            } else {
                return null;
            }
        }
    });

    function initDatepickerLocale(dateLocaleProvider) {

        dateLocaleProvider.months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        dateLocaleProvider.shortMonths = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
        dateLocaleProvider.days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
        dateLocaleProvider.shortDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

        // Can change week display to start on Monday.
        dateLocaleProvider.firstDayOfWeek = 0;

        // Example uses moment.js to parse and format dates.
        dateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD.MM.YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };

        dateLocaleProvider.formatDate = function (date) {
            var m = moment(date);
            return m.isValid() ? m.format('DD.MM.YYYY') : '';
        };

        dateLocaleProvider.monthHeaderFormatter = function (date) {
            return dateLocaleProvider.shortMonths[date.getMonth()] + ' ' + date.getFullYear();
        };

        // In addition to date display, date components also need localized messages
        // for aria-labels for screen-reader users.

        dateLocaleProvider.weekNumberFormatter = function (weekNumber) {
            return 'Неделя ' + weekNumber;
        };

        dateLocaleProvider.msgCalendar = 'Календарь';
        dateLocaleProvider.msgOpenCalendar = 'Открыть календарь';

        // You can also set when your calendar begins and ends.
        dateLocaleProvider.firstRenderableDate = new Date(1776, 6, 4);
        dateLocaleProvider.lastRenderableDate = new Date();
    }
})();