﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .controller('singinController', controller);

    controller.$inject = ['$scope', '$location', '$rootScope', '$timeout', 'auth', 'catchService'];

    function controller($scope, $location, $rootScope, $timeout, auth, catchService) {
        $scope.title = 'singinController';

        $scope.loginForm = {
            login: "",
            password: ""
        };

        $scope.login = function () {
            auth.login($scope.loginForm).then(function () {
                $location.path('/');
            }, function (error) {
                catchService.catchException(error, '', 'Ошибка авторизации');
            });
        };
    }
})();

