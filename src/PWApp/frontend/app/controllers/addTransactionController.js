﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .controller('addTransactionController', addTransactionController);

    addTransactionController.$inject = ['$scope', 'ajaxServiceFactory', 'Notification', 'auth', 'catchService', '$routeParams'];

    function addTransactionController($scope, ajaxServiceFactory, notification, auth, catchService, $routeParams) {
        $scope.title = 'addTransactionController';
        var currentUserId = auth.authentication.userId;
        var bal = auth.authentication.balance;
        var currentUserBalance = isNaN(bal)?bal.replace(/\s/g, ''):bal;

        activate();

        var _userService, _trnService;

        function activate() {
            $scope.loadUsers = _loadUsers;
            $scope.confirm = _confirm;

            $scope.transaction = {};

            _userService = ajaxServiceFactory.getService('api/User');
            _trnService = ajaxServiceFactory.getService('api/Transaction');
            if ($routeParams.receiverId) {
                _loadUsers();
                $scope.receiverId = $routeParams.receiverId;
                $scope.transaction.amount = $routeParams.amount;
            }
        }

        /**
         * LoadAllUsers
         * @returns {} nothing
         */
        function _loadUsers() {
            _userService.query(
                // success callback
                function(response) {
                    if (response.length <= 1) {
                        $scope.data = undefined;
                        notification.warning({ message: 'Нет ни одно пользователя с которым можно провести транзакцию.', title: 'Загрузка пользователей' });
                        $scope.allowTransaction = false;
                    } else {
                        $scope.allowTransaction = true;
                        $scope.users = new Array();
                        response.forEach(function(item, i, arr) {
                            if (item.id !== currentUserId) {
                                $scope.users.push(item);
                            }
                        });
                    }
                },
                // error callback
                function(error) {
                    catchService.catchException(createErrorObject(error), '', 'Ошибка загрузки данных');
                });
        }

        /**
         * Confirm transaction
         * @returns {} 
         */
        function _confirm() {
            if (parseFloat(currentUserBalance) < parseFloat($scope.transaction.amount.replace(/\s/g, ''))) {
                notification.error({
                    message: 'Не достаточно средств для проведения транзакции.',
                    title: 'Подтверждение транзакции'
                });
                return;
            }
            $scope.transaction.toUserId = $scope.receiverId;
            $scope.transaction.correspondentId = currentUserId;
            $scope.transaction.correspondentName = auth.authentication.userName;

            _trnService.save($scope.transaction,
                // success response
                function() {
                    auth.updateBalance();
                    catchService
                        .catchSuccessWithRedirect("/transactions", 'Транзакция успешно проведена', 'Транзакция');
                },
                // error callback
                function(error) {
                    catchService.catchException(createErrorObject(error), '', 'Ошибка проведения транзакции');
                });
        }
    }
})();
