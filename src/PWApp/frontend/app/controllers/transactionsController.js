﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .controller('transactionsController', transactionsController);

    transactionsController.$inject = ['addictionAjaxServiceFactory', 'paginationFactory', 'catchService', '$scope', 'auth', 'Notification', '$window'];

    function transactionsController(addictionAjaxServiceFactory, paginationFactory, catchService, $scope, auth, notification, $window) {
        $scope.title = 'transactionsController';

        activate();

        var trnService;

        function activate() {
            $scope.sort = {
                reverseSort: true,
                orderByField: 'date'
            };

            $scope.currentUserId = auth.authentication.userId;
            $scope.copy = _copy;

            $scope.data = {};

            $scope.pagination = paginationFactory;

            $scope.pagination.pageChanged = _pageChanged;

            $scope.pagination.pageSize = 12;

            trnService = addictionAjaxServiceFactory.getService('/api/Transaction', "/ByUser");
            _showData();
        }

        /**
         * Событие смены страницы
         */
        //TODO: если нужно это событие смены страницы
        function _pageChanged() {

        };

        function _showData() {
            $scope.data = trnService.byOwner({ ownerId: auth.authentication.userId },
                // success callback
                function (response) {
                    if (response.length > 0) {
                        _refreshPagination(false);
                    } else {
                        $scope.data = undefined;
                        notification.warning({ message: 'Нет данных.', title: 'Загрузка транзакций' });
                    }
                },
                // error callback
                function(error) {
                    catchService.catchException(createErrorObject(error), '', 'Ошибка загрузки транзакций');
                });
        }

        function _refreshPagination(newItem) {
            $scope.pagination.totalPages = $scope.data.length;
            if (!newItem) {
                $scope.pagination.currentPage = 1;
            }
        }

        function _copy(trn) {
            var amount = Math.abs(parseFloat(trn.amount));
            $window.location.href = '/#!/addTransaction?receiverId=' + trn.toUserId + '&amount=' + amount;
        }
    }
})();
