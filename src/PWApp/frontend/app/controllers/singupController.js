﻿(function () {
    'use strict';

    angular
        .module('pwApp')
        .controller('singupController', controller);

    controller.$inject = ['$scope', '$location', '$rootScope', '$timeout', 'auth', 'catchService'];

    function controller($scope, $location, $rootScope, $timeout, auth, catchService) {
        $scope.title = 'singupController';

        $scope.newUser = {
            name: "",
            email: "",
            password: "",
            balance: 500
        };

        $scope.save = function () {
            auth.registration.save($scope.newUser).then(function () {
                auth.logOut();
                catchService.catchSuccessWithRedirect("/singin", 'Регистрация прошла успешно', 'Регистрация');
            }, function (error) {
                catchService.catchException(createErrorObject(error), '', 'Ошибка регистрации');
            });
        };
    }
})();

