﻿/**
 * Создает объект ошибки
 * @param {} response ответ от сервера
 * @param {} type тип ошибки (warn/err)
 * @returns {} 
 */
function createErrorObject(response, type) {
    "use strict";
    try {
        return {
            success: false,
            msgType: response === null ? 'err' : type,
            msg: ((response === null || response === undefined || response.status === -1)
                ? "Connection timeout expired. Возможны неполадки на сервере, повторите попытку позже или воспользуйтесь формой обратной связи для обращения в техподержку."
                : response.status === 500
                    ? "Внутренняя ошибка сервера"
                    : (response.data ||
                        response.resultMessage ||
                        response.errors ||
                        (response.error_description
                            ? response.error_description
                            : response)))
        };
    } catch (er) {
        return {
            success: false,
            msgType: 'err',
            msg: "Неизвестная ошибка. Возможны неполадки на сервере, повторите попытку позже или воспользуйтесь формой обратной связи для обращения в техподержку."
        };
    }
};