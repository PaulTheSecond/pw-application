﻿namespace PWApp.Api.Common.Contracts
{
    public interface IDataProviderFactory
    {
        IUnitOfWork Create();
    }
}
