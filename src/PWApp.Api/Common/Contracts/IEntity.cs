﻿namespace PWApp.Api.Common.Contracts
{
    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; set; }
    }

    public interface IEntity
    {
        object GetId();
    }
}
