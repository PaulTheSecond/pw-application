﻿using PWApp.Api.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PWApp.Api.Common.Contracts
{
    public interface IAuthorization<TEntity> where TEntity : Entity<Guid>
    {
        /// <summary>
        /// Retrieve a single item using it's primary key, exception if not found
        /// </summary>
        /// <param name="email">user email</param>
        /// <returns>T</returns>
        TEntity FIndByEmail(string email);
    }
}
