﻿using System;

namespace PWApp.Api.Common.Exceptions
{
    /// <summary>
    /// Исключение, произошедшее в бизнес логике
    /// </summary>
    public class BusinessException : Exception
    {
        public BusinessException()
        {

        }

        public BusinessException(string message) : base(message)
        {

        }

        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
