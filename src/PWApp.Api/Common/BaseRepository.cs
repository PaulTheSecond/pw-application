﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Extensions;

namespace PWApp.Api.Common
{
    /// <summary>
    /// Base class for all SQL based service classes
    /// </summary>
    /// <typeparam name="TEntity">The domain object type</typeparam>
    /// <typeparam name="TU">The database object type</typeparam>
    /// <remarks>from http://www.codeproject.com/Articles/688929/Repository-Pattern-and-Unit-of </remarks>
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>, IDisposable where TEntity : class, IEntity
    {
        internal DbContext Context;
        internal DbSet<TEntity> DbSet;

        public BaseRepository(DbContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            this.Context = context;
            this.DbSet = Context.Set<TEntity>();
        }

        /// <summary>
        /// Returns the object with the primary key specifies or the default for the type
        /// </summary>
        /// <param name="primaryKey">The primary key</param>
        /// <returns>The result mapped to the specified type</returns>
        public TEntity GetById(object primaryKey)
        {
            return DbSet.Find(primaryKey);
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy?.Invoke(query).ToList() ?? query.ToList();
        }

        public virtual bool Exists(object primaryKey)
        {
            return DbSet.Find(primaryKey) != null;
        }

        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Insert(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public void UpdateOrInsert(Expression<Func<TEntity, object>> identifierExpression, Expression<Func<TEntity, object>> updatingExpression, params TEntity[] entities)
        {
            Context.Upsert<TEntity>(identifierExpression, updatingExpression, entities);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        public virtual void Delete(object primaryKey)
        {
            TEntity entityToDelete = DbSet.Find(primaryKey);
            Delete(entityToDelete);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.AsEnumerable();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
