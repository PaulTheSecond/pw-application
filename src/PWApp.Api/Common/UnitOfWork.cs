﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PWApp.Api.Common.Contracts;
using PWApp.Api.DataAccess;

namespace PWApp.Api.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PWAppContext _context;

        public UnitOfWork(PWAppContext context)
        {
            _context = context;
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public DbContext Db => _context;
    }
}
