﻿using System;
using AutoMapper;
using PWApp.Api.Domain;
using PWApp.Api.Dto;

namespace PWApp.Api.Mapping
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            // User -> UserDto
            CreateMap<User, UserDto>();

            // UserDto -> User
            CreateMap<UserDto, User>()
                .ForMember(dest=>dest.Id, map=>map.Condition(cond=>cond.Id != Guid.Empty));
        }
    }
}
