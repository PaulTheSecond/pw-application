﻿using System;
using AutoMapper;
using PWApp.Api.Domain;
using PWApp.Api.Dto;

namespace PWApp.Api.Mapping
{
    public class TransactionProfile: Profile
    {
        public TransactionProfile()
        {
            // Transaction -> TransactionDto
            CreateMap<Transaction, TransactionDto>();

            // TransactionDto -> Transaction
            CreateMap<TransactionDto, Transaction>()
                .ForMember(dest=>dest.Id, map=>map.Condition(cond=>cond.Id != Guid.Empty));
        }
    }
}
