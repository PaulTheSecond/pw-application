﻿using System;
using System.Linq;
using System.Threading;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Extensions;
using PWApp.Api.Domain;
using PWApp.Api.Services;

namespace PWApp.Api.Tools
{
    public static class DbInitializer
    {
        public static void Initialize(IDataProviderFactory dataProviderFactory, IUserService userService,
            ITransactionService transactionService)
        {
            using (var uow = dataProviderFactory.Create())
            {
                uow.Db.Database.EnsureCreated();

                if (userService.GetAll().Any())
                {
                    return;
                }
            }
            var users = new User[]
            {
                new User("Test1@test.ru", "123") {Name = "Test1", Id = Guid.NewGuid(), Balance = 500},
                new User("Test2@test.ru", "123") {Name = "Test2", Id = Guid.NewGuid(), Balance = 500}
            };


            users.ForEach(f => userService.Create(f));

            var transactions = new Transaction[]
            {
                new Transaction()
                {
                    ToUserId = users[1].Id,
                    FromUserId = users[0].Id,
                    CorrespondentId = users[0].Id,
                    Amount = 100,
                    ResultBalance = users[0].Balance - 100,
                    Id = Guid.NewGuid(),
                    Date = DateTime.Now
                },
                new Transaction()
                {
                    ToUserId = users[0].Id,
                    FromUserId = users[1].Id,
                    CorrespondentId = users[1].Id,
                    Amount = 500,
                    ResultBalance = users[0].Balance + 500,
                    Id = Guid.NewGuid(),
                    Date = DateTime.Now
                }
            };

            transactions.ForEach(f =>
            {
                Thread.Sleep(1000);
                transactionService.CreatePair(f);
            });


        }
    }
}
