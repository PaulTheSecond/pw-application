﻿using System;
using System.Collections.Generic;
using System.Linq;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Exceptions;
using PWApp.Api.DataAccess;
using PWApp.Api.Domain;
using PWApp.Api.Services.Common;

namespace PWApp.Api.Services
{
    public interface ITransactionService : ICrudService<Transaction>
    {
        void CreatePair(Transaction entity);
    }

    public class TransactionService : AbstractCrudService<Transaction>, ITransactionService
    {
        #region Fields

        private readonly IUserService _userService;
        
        #endregion
        #region Constructors

        public TransactionService(IDataProviderFactory dataProviderFactory, IUserService userService) : base(dataProviderFactory)
        {
            _userService = userService;
        }

        #endregion
        #region ITransactionService Implementation

        public override bool CheckIfExist(Transaction entity)
        {
            if (entity == null)
            {
                throw new BusinessException("Запись не должна быть пустой");
            }
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new PWAppRepository<Transaction>(unitOfWork.Db);
                return Repository.Get(f => f.Amount == entity.Amount && f.Date==entity.Date && f.CorrespondentId == entity.CorrespondentId).Any();
            }
        }

        public void CreatePair(Transaction entity)
        {
            var trn = entity;

            var corr = _userService.FindBy(f => f.Id == trn.CorrespondentId).FirstOrDefault();

            if (corr.Balance < entity.Amount)
            {
                throw new BusinessException("Недостаточно средств для совершения транзакции.");
            }
            var receiver = _userService.FindBy(f => f.Id == trn.ToUserId).FirstOrDefault();
            corr.Balance += -trn.Amount;
            receiver.Balance += trn.Amount;
            
            trn.ResultBalance = receiver.Balance;
            trn.FromUserId = corr.Id;
            trn.CorrespondentName = corr.ToString();
            trn.Date = DateTime.Now;

            var corrTrn = new Transaction
            {
                Amount = -trn.Amount,
                CorrespondentId = trn.CorrespondentId,
                CorrespondentName = corr.ToString(),
                FromUserId = trn.CorrespondentId,
                ToUserId = receiver.Id,
                ResultBalance = corr.Balance,
                Date = DateTime.Now
            };

            var trns = new[] {trn, corrTrn};

            foreach (var transaction in trns)
            {
                Create(transaction);
            }

            _userService.Update(corr);
            _userService.Update(receiver);
        }

        #endregion
    }
}
