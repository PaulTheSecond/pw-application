﻿using System.Linq;
using PWApp.Api.Common.Contracts;
using PWApp.Api.Common.Exceptions;
using PWApp.Api.DataAccess;
using PWApp.Api.Domain;
using PWApp.Api.Services.Common;

namespace PWApp.Api.Services
{
    public interface IUserService : ICrudService<User>
    {
    }

    public class UserService : AbstractCrudService<User>, IUserService, IAuthorization<User>
    {
        #region Constructors

        public UserService(IDataProviderFactory dataProviderFactory) : base(dataProviderFactory)
        {
        }

        #endregion
        #region IUserService Implementation

        public override bool CheckIfExist(User entity)
        {
            if (entity == null)
            {
                throw new BusinessException("Запись не должна быть пустой");
            }
            if (string.IsNullOrWhiteSpace(entity.Email))
            {
                throw new BusinessException("Не заполнено обязательное поле Email");
            }
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new PWAppRepository<User>(unitOfWork.Db);
                return Repository.Get(f => f.Email == entity.Email).Any();
            }
        }

        #endregion
        #region IAuthorization<User> Implementation

        public User FIndByEmail(string email)
        {
            return FindBy(f => f.Email == email).FirstOrDefault();
        }

        #endregion
    }
}
