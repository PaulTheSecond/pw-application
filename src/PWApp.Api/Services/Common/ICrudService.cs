﻿using System;
using System.Collections.Generic;
using PWApp.Api.Common;
using PWApp.Api.Common.Contracts;

namespace PWApp.Api.Services.Common
{
    public interface ICrudService<TEntity> : IService where TEntity : Entity<Guid>
    {
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(object id);
        IList<TEntity> GetAll();
        bool CheckIfExist(TEntity entity);
        IEnumerable<TEntity> FindBy(System.Linq.Expressions.Expression<Func<TEntity, bool>> expression);
    }
}
