﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PWApp.Api.Common;
using PWApp.Api.Common.Contracts;

namespace PWApp.Api.DataAccess
{
    public class PWAppDataProviderFactory : IDataProviderFactory
    {
        private PWAppContext context;
        private readonly DbContextOptionsBuilder<PWAppContext> _options;

        public PWAppDataProviderFactory(IConfiguration conf)
        {
            var connString = conf.GetConnectionString("pw-database");
            _options = new DbContextOptionsBuilder<PWAppContext>();
            _options.UseSqlServer(connString);
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(new PWAppContext(_options.Options));
        }
    }
}
