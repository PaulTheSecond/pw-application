﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using PWApp.Api.Domain;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PWApp.Api.DataAccess
{
    public class PWAppContext : DbContext
    {
        #region Constructors
        
        public PWAppContext(DbContextOptions<PWAppContext> options) : base(options)
        {
        }

        #endregion
        #region Properties

        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        #endregion
        #region DbContext Implementation

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(i => i.Email)
                .IsUnique();

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var validationErrors = ChangeTracker
                .Entries<IValidatableObject>()
                .SelectMany(e => e.Entity.Validate(null))
                .Where(r => r != ValidationResult.Success);

            if (validationErrors.Any())
            {
                Exception raise = null;
                
                    foreach (var validationError in validationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationError.MemberNames.Aggregate(string.Empty, (res, name) => res += name + "," ),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message);
                    }
                throw raise;
            }

            return base.SaveChanges();
        }

        #endregion
    }
}
