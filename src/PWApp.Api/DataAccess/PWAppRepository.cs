﻿using Microsoft.EntityFrameworkCore;
using PWApp.Api.Common;
using PWApp.Api.Common.Contracts;

namespace PWApp.Api.DataAccess
{
    /// <summary>
    /// Обобщенный репозиторий для доступа к БД Pamo
    /// </summary>
    /// <typeparam name="TEntity">сущность домена</typeparam>
    public class PWAppRepository<TEntity> : BaseRepository<TEntity> where TEntity : class, IEntity
    {
        public PWAppRepository(DbContext context) : base(context)
        {
        }
    }
}
