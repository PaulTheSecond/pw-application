﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using PWApp.Api.Common;

namespace PWApp.Api.Domain
{
    [Description("Пользователь")]
    public class User : Entity<Guid>, IValidatableObject
    {
        #region Constructors

        public User(string email, string password)
        {
            this.Email = email;
            ChangePassword(password);
        }

        public User()
        {
        }

        #endregion
        #region Properties
        
        [Required]
        public virtual string Name { get; set; }

        [MaxLength(128)]
        [Required]
        public virtual string Email { get; set; }

        [Required]
        public virtual decimal Balance { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [MaxLength(48)]
        [Required]
        public string Password { get; private set; }

        /// <summary>
        /// Соль
        /// </summary>
        [MaxLength(24)]
        [Required]
        public string Salt { get; private set; }

        #endregion
        #region Methods

        /// <summary>
        /// Проверяем пароль пользователя
        /// </summary>
        /// <param name="password">проверяемый пароль</param>
        /// <returns></returns>
        public bool IsValidPassword(string password)
        {
            var hash = HashPassword(password);
            return hash == Password;
        }

        /// <summary>
        /// Генерация соли
        /// </summary>
        /// <returns>Соль</returns>
        /// <remarks> по информации с http://stackoverflow.com/questions/4181198/how-to-hash-a-password </remarks>
        private static string GenerateSalt()
        {
            var salt = new byte[16];

            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(salt);

            return Convert.ToBase64String(salt);
        }

        /// <summary>
        /// Поиск хэша пароля
        /// </summary>
        /// <param name="password">пароль</param>
        /// <returns>хэш</returns>
        /// <remarks> по информации с http://stackoverflow.com/questions/4181198/how-to-hash-a-password </remarks>
        private string HashPassword(string password)
        {
            var salt = Convert.FromBase64String(Salt);

            var r = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = r.GetBytes(32);
            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Сменить пароль32/2
        /// </summary>
        /// <param name="newPassword">Новый пароль</param>
        public void ChangePassword(string newPassword)
        {
            Salt = GenerateSalt();
            Password = HashPassword(newPassword);
        }

        public override string ToString()
        {
            return Name ?? Email;
        }

        #endregion
        #region IValidatableObject Implementation

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateProperty(this.Name,
                new ValidationContext(this, null, null) { MemberName = "Name" },
                results);
            Validator.TryValidateProperty(this.Balance,
                new ValidationContext(this, null, null) { MemberName = "Balance" },
                results);
            Validator.TryValidateProperty(this.Email,
                new ValidationContext(this, null, null) { MemberName = "Email" },
                results);
            Validator.TryValidateProperty(this.Password,
                new ValidationContext(this, null, null) { MemberName = "Password" },
                results);
            Validator.TryValidateProperty(this.Salt,
                new ValidationContext(this, null, null) { MemberName = "Salt" },
                results);
            Validator.TryValidateProperty(this.Id,
                new ValidationContext(this, null, null) { MemberName = "Id" },
                results);
            return results;
        }

        #endregion
    }
}
