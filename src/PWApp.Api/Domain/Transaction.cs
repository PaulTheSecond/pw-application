﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PWApp.Api.Common;

namespace PWApp.Api.Domain
{
    [Description("Транзакция")]
    public class Transaction:Entity<Guid>, IValidatableObject
    {
        #region Properties

        [Required]
        public virtual DateTime Date { get; set; }
        
        public virtual Guid CorrespondentId { get; set; }
        
        public virtual string CorrespondentName { get; set; }
        
        public virtual Guid FromUserId { get; set; }

        [ForeignKey("FromUserId")]
        public virtual User FromUser { get; set; }
        
        public virtual Guid ToUserId { get; set; }

        [ForeignKey("ToUserId")]
        public virtual User ToUser { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        [Required]
        public virtual decimal ResultBalance { get; set; }

        #endregion
        #region IValidatableObject Implementation

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateProperty(this.ToUser,
                new ValidationContext(this, null, null) {MemberName = "ToUser" },
                results);
            Validator.TryValidateProperty(this.FromUser,
                new ValidationContext(this, null, null) {MemberName = "FromUser" },
                results);
            Validator.TryValidateProperty(this.Amount,
                new ValidationContext(this, null, null) {MemberName = "Amount"},
                results);
            Validator.TryValidateProperty(this.ResultBalance,
                new ValidationContext(this, null, null) {MemberName = "ResultBalance"},
                results);
            Validator.TryValidateProperty(this.CorrespondentName,
                new ValidationContext(this, null, null) {MemberName = "CorrespondentName" },
                results);
            Validator.TryValidateProperty(this.Date,
                new ValidationContext(this, null, null) {MemberName = "Date"},
                results);
            Validator.TryValidateProperty(this.Id,
                new ValidationContext(this, null, null) {MemberName = "Id"},
                results);
            return results;
        }

        #endregion
    }
}
