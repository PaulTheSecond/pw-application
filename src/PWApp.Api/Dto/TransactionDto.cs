﻿using System;

namespace PWApp.Api.Dto
{
    /// <summary>
    /// Transaction DTO
    /// </summary>
    public class TransactionDto
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }
        
        public string CorrespondentName { get; set; }
        
        public Guid ToUserId { get; set; }
        
        public Guid FromUserId { get; set; }

        public Guid CorrespondentId { get; set; }
        
        public decimal Amount { get; set; }
        
        public decimal ResultBalance { get; set; }
    }
}
