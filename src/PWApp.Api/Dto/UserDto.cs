﻿using System;

namespace PWApp.Api.Dto
{
    /// <summary>
    /// User DTO
    /// </summary>
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Email { get; set; }

        public string Password { get; set; }

        public decimal Balance { get; set; }
    }
}
